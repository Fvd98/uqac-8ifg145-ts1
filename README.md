<h1>Travail de session numéro 1</h1>

À partir de votre projet de conception d’un jeux ou développement d’un système démontrer la solidité de votre offre de service en GP,
Commencer par vous faire un plan de projet pour livrer votre travail de session (qui va faire quoi, quand et avec quel effort prévu)


<table>
  <tr>
    <th>Énoncé</th>
    <th>Pondération</th>
  </tr>
  <tr>
    <td>01 - Commande de service</td>
    <td>/5</td>
  </tr>
  <tr>
    <td>02 - Mandat (estimé Excel et plan maître MS-Projet)</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>03 - Offre de service</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>04 - Un RASCI</td>
    <td>/5</td>
  </tr>
  <tr>
    <td>05 - Justification du projet ; qualitative et financière</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>06 - Analyse de risque à priori</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>07 - Analyse des parties prenantes</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>08 - Gérer la complexité, démontrer comment la complexité va être gérée dans l’offre de service</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>09 - Démontrer comment les demandes de changement vont être gérées dans l’offre de service.</td>
    <td>/10</td>
  </tr>
  <tr>
    <td>10 - Phase démarrage ; quel sera votre approche en GP (méthodologie)  pour vous assurer un succès de votre projet.</td>
    <td>/20</td>
  </tr>
</table>

